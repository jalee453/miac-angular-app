This app was built using the [Yeoman](http://yeoman.io/) ASP.NET Core Spa Generator.

To run this app locally, you will need to have .NET Core and Node.js (version 6 or later) installed.

Once installed, run the following commands.


    dotnet restore
    npm install 

You will also need Webpack to transpile TypeScript files to JS.

Run the following to install Webpack.

    npm install -g webpack

Then

    webpack --config webpack.config.vendor.js 
    webpack

.. and finally 

    dotnet run

Browse to localhost:5000 to view the app running.
