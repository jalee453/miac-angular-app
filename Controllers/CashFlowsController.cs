using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace WebApplicationBasic.Controllers
{
    [Route("api/[controller]")]
    public class CashFlowsController : Controller
    {
        [HttpGet("[action]")]
        public IEnumerable<CashFlow> GetCashFlows()
        {
            var cashFlow = new CashFlow 
            {
                Month = 1,
                Interest = 13.54,
                Principal = 410.50,
                RemainingBalance = 4589.50
            };

            return new CashFlow[] {cashFlow};
        }

        [HttpPost("[action]")]
        public IEnumerable<CashFlow> AddNewLoan([FromBody] Loan newLoan) {
            List<CashFlow> cashFlows = new List<CashFlow>();
            var rateOfInterest = newLoan.Interest / 1200;
            var paymentAmount = (newLoan.LoanAmount * rateOfInterest) / (1 - Math.Pow(1 + rateOfInterest, newLoan.Term * -1));
            var previousRemainingBalance = newLoan.LoanAmount;

            for (var i = 1; i <= newLoan.Term; i++)
            {
                var interestPayment = previousRemainingBalance * rateOfInterest;
                var principalPayment = paymentAmount - interestPayment;
                var remainingBalance = previousRemainingBalance - principalPayment;

                previousRemainingBalance = remainingBalance;

                cashFlows.Add(new CashFlow 
                {
                    Month = i,
                    Interest = Math.Round(interestPayment, 2),
                    Principal = Math.Round(principalPayment, 2),
                    RemainingBalance = Math.Round(remainingBalance, 2)
                });
            }

            return cashFlows;
        }

        public class Loan 
        {
            public double LoanAmount { get; set; }
            public int Term { get; set;}
            public double Interest { get; set; }
        }

        public class CashFlow
        {
            public int Month { get; set; }
            public double Interest { get; set; }
            public double Principal { get; set; }
            public double RemainingBalance { get; set; }
        }
    }
}
