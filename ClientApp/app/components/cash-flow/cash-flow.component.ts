import { Component, Inject } from '@angular/core';
import { Http } from '@angular/http';

import { CashFlowService } from '../../services/cash-flow.service';

@Component({
    selector: 'cash-flow',
    templateUrl: './cash-flow.component.html'
})

export class CashFlowComponent {
    public cashFlows: any[] = [];
    public poolLevel: any[] = [];

    constructor(private cashFlowService: CashFlowService) { }

    onSubmit(loan) {
        let sumInterest = 0, sumPrincipal = 0, sumRemainingBalance = 0;
        this.cashFlowService.addNewLoan(loan.value).subscribe(cashFlows => {
            this.cashFlows.push(cashFlows);
            if (this.cashFlows.length > 1) {
                let cashFlow = this.cashFlows[0];

                for (let i = 0; i < cashFlow.length; i++) {
                    sumInterest += cashFlow[i].interest
                    sumPrincipal += cashFlow[i].principal;
                    sumRemainingBalance += cashFlow[i].remainingBalance;

                    for (let j = 1; j < this.cashFlows.length; j++) {
                        let nextCashFlow = this.cashFlows[j];
                        sumInterest += nextCashFlow[i].interest;
                        sumPrincipal += nextCashFlow[i].principal;
                        sumRemainingBalance += nextCashFlow[i].remainingBalance;
                    }

                    this.poolLevel.push({
                        month: i+1,
                        interest: sumInterest,
                        principal: sumPrincipal,
                        remainingBalance: sumRemainingBalance
                    });
                }
            }
        });

    }
}