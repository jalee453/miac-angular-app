import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppComponent } from './components/app/app.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { CashFlowComponent } from './components/cash-flow/cash-flow.component';

import { CashFlowService } from './services/cash-flow.service';

export const sharedConfig: NgModule = {
    bootstrap: [ AppComponent ],
    declarations: [
        AppComponent,
        NavMenuComponent,
        CashFlowComponent,
    ],
    providers: [ CashFlowService ],
    imports: [
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: CashFlowComponent },
            { path: '**', redirectTo: 'home' }
        ])
    ]
};
