import { Inject, Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class CashFlowService {
    constructor(private http: Http, @Inject('ORIGIN_URL') private originUrl: string) { }

    addNewLoan(loan) {
        return this.http.post(this.originUrl + '/api/CashFlows/AddNewLoan', loan)
            .map(res => res.json());
    }
}
